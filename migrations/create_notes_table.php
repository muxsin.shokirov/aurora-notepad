<?php

use Muhsin\Aurora\Core\Database;

return Database::getInstance()->query(
    <<<SQL
CREATE TABLE Notes (
    id INT PRIMARY KEY AUTO_INCREMENT,
    uuid VARCHAR(255) UNIQUE,
    title VARCHAR(255),
    color VARCHAR(255),
    content LONGTEXT,
    user_id INT,
    FOREIGN KEY (user_id) REFERENCES Users(id)
)
SQL
);
