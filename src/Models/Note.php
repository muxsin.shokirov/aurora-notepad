<?php

declare(strict_types=1);

namespace Muhsin\Aurora\Models;

use Muhsin\Aurora\Core\Database;

class Note
{
    private Database $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function get(int $id): false|array
    {
        return $this->db->query('SELECT * FROM Notes WHERE id = :id', ['id' => $id])->findOrFail();
    }

    public function getAllByUserId(int $user_id): false|array
    {
        return $this->db->query('SELECT * FROM Notes WHERE user_id = :user_id', ['user_id' => $user_id])->findAll();
    }

    public function create(string $uuid, string $title, string $content, string $color, int $user_id): string|array
    {
        $id = $this->db->query(
            'INSERT INTO Notes (uuid, title, content, color, user_id) VALUES (:uuid, :title, :content, :color, :user_id)',
            ['uuid' => $uuid, 'title' => $title, 'content' => $content, 'color' => $color, 'user_id' => $user_id]
        )->insert();

        if ($id === false) {
            return "Failed to create note.";
        }

        return $this->get($id);
    }

    public function update(int $uuid, string $title, string $content, string $color): bool|string
    {
        $result = $this->db->query(
            'UPDATE Notes SET title=:title, content=:content, color=:color WHERE uuid=:uuid',
            ['title' => $title, 'content' => $content, 'color' => $color, 'uuid' => $uuid]
        )->update();

        if ($result === false) {
            return "Failed to update note.";
        }

        return true;
    }

    public function delete(int $uuid): bool|string
    {
        $result = $this->db->query(
            'DELETE FROM Notes WHERE uuid=:uuid',
            ['id' => $uuid]
        )->delete();

        if ($result === false) {
            return "Failed to update note.";
        }

        return true;
    }

    public function deleteAllByUserId(int $user_id): true|string
    {
        $result = $this->db->query(
            'DELETE FROM Notes WHERE user_id=:user_id',
            ['user_id' => $user_id]
        )->delete();

        if ($result === false) {
            return "Failed to update note.";
        }

        return true;
    }
}
