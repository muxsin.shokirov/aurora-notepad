<?php

declare(strict_types=1);

namespace Muhsin\Aurora\Controllers;

use Muhsin\Aurora\Core\Auth;
use Muhsin\Aurora\Models\Note;

class NoteController
{
    private Note $note_model;
    private Auth $auth;

    public function __construct(Note $note_model, Auth $auth)
    {
        $this->note_model = $note_model;
        $this->auth = $auth;
    }

    public function all(): void
    {
        $header = apache_request_headers();

        if (isset($header['Authorization'])) {
            $token = $header['Authorization'];
            $decode = $this->auth->check($token);

            if ($decode) {
                $user = $this->auth->user($token);

                if ($user !== false) {
                    $result = $this->note_model->getAllByUserId($user['id']);

                    if ($result === false) {
                        http_response_code(400);
                        exit;
                    }

                    echo json_encode([
                        'notes' => $result
                    ]);
                    exit;
                }
            }
        }

        http_response_code(401);
    }

    public function create(): void
    {
        $header = apache_request_headers();

        if (isset($header['Authorization'])) {
            $token = $header['Authorization'];
            $decode = $this->auth->check($token);

            if ($decode) {
                $user = $this->auth->user($token);

                if ($user !== false) {
                    $result = $this->note_model->create($_POST['uuid'], $_POST['title'], $_POST['content'], $_POST['color'], $user['id']);

                    if (is_string($result)) {
                        echo json_encode(['error' => $result]);

                        return;
                    }

                    http_response_code(204);
                    exit;
                }
            }
        }

        http_response_code(401);
    }

    public function update(): void
    {
        $header = apache_request_headers();

        if (isset($header['Authorization'])) {
            $token = $header['Authorization'];
            $decode = $this->auth->check($token);

            if ($decode) {
                $user = $this->auth->user($token);

                if ($user !== false) {
                    $result = $this->note_model->update((int)$_POST['uuid'], $_POST['title'], $_POST['content'], $_POST['color']);

                    if (is_string($result)) {
                        echo json_encode(['error' => $result]);

                        return;
                    }

                    http_response_code(204);
                    exit;
                }
            }
        }

        http_response_code(401);
    }

    public function delete(): void
    {
        $header = apache_request_headers();

        if (isset($header['Authorization'])) {
            $token = $header['Authorization'];
            $decode = $this->auth->check($token);

            if ($decode) {
                $user = $this->auth->user($token);

                if ($user !== false) {
                    $result = $this->note_model->delete((int)$_POST['note_uuid']);

                    if (is_string($result)) {
                        echo json_encode(['error' => $result]);

                        return;
                    }

                    http_response_code(204);
                    exit;
                }
            }
        }

        http_response_code(401);
    }

    public function async(): void
    {
        $header = apache_request_headers();

        if (isset($header['Authorization'])) {
            $token = $header['Authorization'];
            $decode = $this->auth->check($token);

            if ($decode) {
                $user = $this->auth->user($token);

                if ($user !== false) {
                    $data = json_decode(file_get_contents("php://input"));

                    if ($data->notes) {
                        $notes = $data->notes;

                        $delete_result = $this->note_model->deleteAllByUserId($user['id']);

                        // TODO handling delete result

                        foreach ($notes as $note) {
                            $result = $this->note_model->create($note->uuid, $note->title, $note->content, $note->color, $user['id']);

                            if (is_string($result)) {
                                echo json_encode(['error' => $result]);

                                return;
                            }
                        }

                        http_response_code(204);
                        exit;
                    }
                }
            }
        }

        http_response_code(401);
    }
}
