# Aurora Notepad API

## Требование

- PHP 8.2
- MySql DBMS
- Composer

## Установка

1. Запустите `docker-compose up --build -d --remove-orphans`
1. Запустите `composer install` внутри контейнера
1. Запустите `php ./aurora migrate` внутри контейнера

## Запуск

1. Запустите `docker-compose up --build -d --remove-orphans`
1. Откройте коллекцию из репозитория в Postman, чтобы просмотреть примеры и протестировать API.
1. Наслаждайтесь!

## Авторизация

Для авторизации укажите токен в заголовке `Authorization`. Формат: `<token>`

## Конечные точки

### Аутентификации

- `POST /api/authorize`
    - **Доступ**: гость
    - **Body**:
        - `email`: string
        - `password`: string
    - **Описание**: Конечная точка для авторизации пользователя.
- `POST /api/register`
    - **Доступ**: гость
    - **Body**:
        - `email`: string
        - `password`: string
    - **Описание**: Конечная точка для регистрации нового пользователя.
- `GET /api/feed`
    - **Доступ**: авторизованный
    - **Описание**: Конечная точка проверяет наличие валидации токена.

### Заметки

- `GET /api/notes`
    - **Доступ**: авторизованный
    - **Описание**: Конечная точка для получения списка заметок.
- `POST /api/notes`
    - **Доступ**: авторизованный
    - **Body**:
        - `uuid`: string
        - `title`: string
        - `content`: string
        - `color`: string
    - **Описание**: Конечная точка для создания новой заметки.
- `POST /api/notes/update`
    - **Доступ**: авторизованный
    - **Body**:
        - `title`: string
        - `content`: string
        - `color`: string
    - **Описание**: Конечная точка для обновления заметки.
- `POST /api/notes/delete`
    - **Доступ**: авторизованный
    - **Body**:
        - `note_uuid`: integer
    - **Описание**: Конечная точка для удаления заметки.
- `POST /api/notes/async`
    - **Доступ**: авторизованный
    - **Body**:
        - `type`: json
        - `properties`:
          - `notes`: array of objects
          - `properties`:
              - `uuid`: string
              - `title`: string
              - `content`: string
              - `color`: string
    - **Описание**: Конечная точка для синхранизация заметки.

## Дополнительный

- Вы можете запустить `php ./aurora empty <имя таблицы>` внутри контейнера, чтобы полностью удалить данные из таблицы.