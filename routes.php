<?php

declare(strict_types=1);

use Muhsin\Aurora\Controllers\NoteController;
use Muhsin\Aurora\Controllers\UserController;
use Muhsin\Aurora\Core\Auth;
use Muhsin\Aurora\Core\Database;
use Muhsin\Aurora\Core\Route;
use Muhsin\Aurora\Models\Note;
use Muhsin\Aurora\Models\User;

$database = Database::getInstance();

$user_model = new User($database);
$note_model = new Note($database);

$user_controller = new UserController($user_model, Auth::getInstance());
$note_controller = new NoteController($note_model, Auth::getInstance());

return [
    Route::post('/api/register', [$user_controller, 'register']),
    Route::post('/api/authorize', [$user_controller, 'authorize']),
    Route::get('/api/feed', [$user_controller, 'feed']),
    Route::get('/api/notes', [$note_controller, 'all']),
    Route::post('/api/notes', [$note_controller, 'create']),
    Route::post('/api/notes/update', [$note_controller, 'update']),
    Route::post('/api/notes/delete', [$note_controller, 'delete']),
    Route::post('/api/notes/async', [$note_controller, 'async']),
];
