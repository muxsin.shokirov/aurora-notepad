<?php

return [
    'database' => [
        'host' => 'mysql',
        'port' => 3306,
        'charset' => 'utf8mb4',
        'database' => 'aurora',
        'username' => 'aurora',
        'password' => 'secret',
    ],
    'secret_key' => 'aurora_notepad'
];
